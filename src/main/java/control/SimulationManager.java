package src.main.java.control;


import java.util.*;

import src.main.java.model.Task;
import src.main.java.view.ShopGUI;
import src.main.java.view.SimulatorFrame;

public class SimulationManager implements Runnable {

	public int timeLimit = 100;
	public int maxProcessingTime = 10;
	public int minProcessingTime = 2;
	public int numberOfServers = 3;
	public int numberOfClients = 100;
	private ShopGUI shop;
	
	private SimulatorFrame sf;
	//private Scheduler scheduler = new Scheduler(numberOfServers, numberOfClients);
	ArrayList<Task> generatedTasks = new ArrayList<Task>();
	Scheduler scheduler;

	public SimulationManager(ShopGUI shop,int timeLimit, int maxProcessingTime, int minProcessingTime, int numberOfServers,int numberOfClients) {
		
		this.shop = shop;
		this.timeLimit = timeLimit;
		this.maxProcessingTime = maxProcessingTime;
		this.minProcessingTime = minProcessingTime;
		this.numberOfClients = numberOfClients;
		this.numberOfServers = numberOfServers;
		scheduler = new Scheduler(numberOfServers, numberOfClients);
		shop = new ShopGUI();
		generateRandomTasks();
		printGeneratedTasks();
		
		
	}
	

	public void run() {
		int currentTime = 0;
		shop.displayServers(scheduler.getServer(), numberOfServers);
		shop.displayTime(currentTime);

		while (currentTime < timeLimit) {
			shop.displayServers(scheduler.getServer(), numberOfServers);
		//	shop.displayTime(currentTime);
	;
			for (Task t : generatedTasks) {
				if(t.getArrivalTime() == currentTime) {
					scheduler.dispatchTask(t);
					t = null;
					shop.displayServers(scheduler.getServer(), numberOfServers);
				//	shop.displayTime(currentTime);
				}
			}
			
			System.out.println("Time: " + currentTime);
			currentTime++;
			try {
				Thread.sleep(1000);
				shop.displayServers(scheduler.getServer(), numberOfServers);
				shop.displayTime(currentTime);
			} catch (InterruptedException e) {
				System.out.println("Error at SimulationManager/Run");
				e.printStackTrace();
			}
		}

	}

	public void generateRandomTasks() {
		
		int id;
		int arrivalTime;
		int processingTime;
		Random rand = new Random();
		for (int i = 0; i < numberOfClients; i++) {
			
			id = i + 1;
			arrivalTime = rand.nextInt(timeLimit) + 1;
			processingTime = rand.nextInt(maxProcessingTime + 1 - minProcessingTime) + minProcessingTime;
			this.generatedTasks.add(new Task (arrivalTime,processingTime,id));
			//System.out.println(generatedTasks.get(i).toString());
		}
		
		sortGeneratedTasks();
	}
	
	public List<Task> getTasksList(){
		return this.generatedTasks;
	}
	
	public void sortGeneratedTasks() {
		
		Collections.sort(generatedTasks);
	}
	
	public void printGeneratedTasks() {
		
		for(Task t : generatedTasks) {
			System.out.println(t.toString());
		}
	}

}
