package src.main.java.view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class SimulatorFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel panel;
	private int WIDTH = 600, HEIGHT = 600;
	JTextField timer = new JTextField();
	
	
	public SimulatorFrame(){
		panel = new JPanel();
		this.add(panel);
		this.setSize(WIDTH, HEIGHT);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	public void displayData(int time){
		System.out.println("print ui");
		panel.removeAll();
		panel.revalidate();
		timer.setText(Integer.toString(time));
		panel.add(timer);
		panel.repaint();
		panel.revalidate();
	}
}
