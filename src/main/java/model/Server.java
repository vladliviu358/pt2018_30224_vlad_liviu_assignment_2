package src.main.java.model;



import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable {

	private LinkedList<Task> tasks = new LinkedList<Task>();
	private AtomicInteger waitingPeriod = new AtomicInteger();
	private int id;
	
	public Server() {
		
	}
	public Server(int id) {

		this.setId(id);
	}
	
	public String toString()
	{
		String s="";
		for(int i=0; i<this.tasks.size(); i++)
		{
			if(this.tasks.size()!=0)
			s+=this.tasks.get(i).toString();
		}
		return s;
	}

	public void addTask(Task task) {

		tasks.add(task);
		waitingPeriod.getAndAdd(task.getProcessingTime());
	}

	public void removeTask(Task task) {

		tasks.remove(task);
	}

	public LinkedList<Task> getList() {

		return this.tasks;
	}

	public void run() {

	
		while (true) {

			try {

				if (this.tasks.size() != 0) {

					Thread.sleep(this.tasks.get(0).getProcessingTime() * 1000);
					//System.out.println("Clientul cu id " + this.tasks.get(0).getId() + " a ajuns la casa");
					for (int i = 0; i < this.tasks.get(0).getProcessingTime(); i++) {
						//System.out.println(getWaitingPeriod());
						waitingPeriod.decrementAndGet();
					}
					System.out.println("Clientul cu id " + this.tasks.get(0).getId() + " a fost servit");
					// System.out.println(tasks.get(0).getProcessingTime());
					this.tasks.remove(0);
				} else
					Thread.sleep(1000);
			}

			catch (Exception e) {
				System.out.println("Error on Server/Run");
				e.printStackTrace();
			}
		}

	}


	public int getWaitingPeriod() {

		return waitingPeriod.get();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	

}
