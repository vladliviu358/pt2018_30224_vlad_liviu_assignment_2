package src.main.java.control;

import java.util.ArrayList;
import java.util.List;

import src.main.java.model.Server;
import src.main.java.model.Task;

public class Scheduler extends Thread {

	private ArrayList<Server> servers = new ArrayList();
	private int maxNoServers;
	private int maxTasksPerServer;
	private Task task;

	public Scheduler(int maxNoServers, int maxTasksPerServer) {

		this.maxNoServers = maxNoServers;
		this.maxTasksPerServer = maxTasksPerServer;
		for (int i = 0; i < maxNoServers; i++) {
			this.servers.add(new Server(i));
			System.out.println("Server " + servers.get(i).getId() + " created!");
			(new Thread(servers.get(i))).start();
		}
	}

	public ArrayList<Server> getServer() {

		return this.servers;

	}
	
	public int findServerWithMinWaitingTime() {
		
		int minWaitingTime = 0;
		for (int i = 0; i<maxNoServers; i++) {
			if(servers.get(i).getWaitingPeriod() < servers.get(minWaitingTime).getWaitingPeriod() && servers.size() != 0) {
				minWaitingTime = i;
			}
		}
		
		return minWaitingTime;
	}
	
	public int findEmptyServer() {
		
		int emptyServer = -1;
		
		for (int i = 0; i<maxNoServers;i++) {
			
			if(this.servers.get(i).getList().size() == 0) {
				
				emptyServer = i;
				break;
			}
		}
		return emptyServer;
	}
	
	public void dispatchTask(Task t) {
	
		
		int emptyServer = findEmptyServer();
		int minWaitingTime = findServerWithMinWaitingTime();
		System.out.println("Clientul cu id " + t.getId() + " a ajuns la casa " + this.servers.get(minWaitingTime).getId());
			this.servers.get(minWaitingTime).addTask(t);
		
		}
			
		//this.servers.get(minWaitingTime).addTask(t);
	}
	
	
	

