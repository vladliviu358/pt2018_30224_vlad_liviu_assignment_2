package src.main.java.model;


import java.util.*;


public class Task implements Comparable<Task> {

	private int arrivalTime;
	private int processingTime;
	private int id;

	public Task(int arrivalTime, int processingTime, int id) {

		this.setArrivalTime(arrivalTime);
		this.setProcessingTime(processingTime);
		this.setId(id);

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getProcessingTime() {
		return processingTime;
	}

	public void setProcessingTime(int processingTime) {
		this.processingTime = processingTime;
	}

	public int getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public int getFinishTime() {

		return this.arrivalTime + this.processingTime;
	}

	public String toString() {

		String info = "";
		info = "Clientul " + this.id + " a ajuns la secunda " + this.arrivalTime + " si are un timp de procesare de "
				+ this.processingTime + " secunde";
		String info1 = "";
		info1 = "ID:" + this.id + " [" + this.arrivalTime + "," + this.processingTime +"] ";

		return info1;
	}

	public int compareTo(Task task) {
		
		if (this.arrivalTime<task.arrivalTime)
		 { 
			 return -1;
		 }
	     if (this.arrivalTime>task.arrivalTime)
	     {
	    	 return 0;
	     }  
	     return 1;
	}

}
