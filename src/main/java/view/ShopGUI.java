package src.main.java.view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.TextArea;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.SystemColor;
import java.awt.Label;
import java.awt.event.ActionListener;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JScrollBar;
import javax.swing.JPanel;
import javax.swing.JTextPane;

import src.main.java.control.SimulationManager;
import src.main.java.model.Server;

public class ShopGUI {

	public JFrame frmShop;
	
	private JTextField time;
	private JTextField ptMax;
	private JTextField ptMin;
	private JTextField serverNo;
	private JTextField clientsNo;
	private JTextField tempT;
	private JLabel temp;

	private ArrayList<JLabel> lblServer = new ArrayList<JLabel>();
	private ArrayList<JTextField> serverTextField = new ArrayList<JTextField>();
	private JTextField timer;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ShopGUI window = new ShopGUI();
					window.frmShop.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ShopGUI() {
		initialize();
	}
	

	public void loadSimManager(int timeLimit, int maxProcessingTime, int minProcessingTime, int numberOfServers,
			int numberOfClients) {

		SimulationManager simManager = new SimulationManager(this,timeLimit, maxProcessingTime, minProcessingTime,
				numberOfServers, numberOfClients);
		
		Thread t = new Thread(simManager);
		t.start();
		
		
	}
	
	public void update() {
		frmShop.revalidate();
		frmShop.repaint();
	}
	
	
	public void drawServers(int numberOfServers) {
		
		for (int i = 0; i < numberOfServers; i++) {

			temp = new JLabel("Casa: " + Integer.toString(i + 1));
			temp.setFont(new Font("Arial Black", Font.PLAIN, 14));
			temp.setBounds(100, 265 + i * 30, 65, 25);
			frmShop.getContentPane().add(temp);
			lblServer.add(temp);

			tempT = new JTextField();
			tempT.setColumns(10);
			tempT.setBounds(200, 265 + i * 30, 400, 25);
			tempT.setFont(new Font("Arial Black", Font.PLAIN, 14));
			tempT.setEditable(false);
			frmShop.getContentPane().add(tempT);
			serverTextField.add(tempT);

		}
	}
	
	public void displayTime(int time) {
		
		Thread t1 = new Thread();
		t1.start();
		update();
		timer.setText(Integer.toString(time));
	}
	
	public void displayServers(ArrayList<Server> servers, int numberOfServers) {
		
		for (int i = 0; i<numberOfServers; i++) {
			serverTextField.get(i).setText(""+ servers.get(i).toString());
		}
	}
	
		/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmShop = new JFrame();
		frmShop.getContentPane().setBackground(new Color(204, 255, 204));
		frmShop.setTitle("Shop");
		frmShop.setBounds(100, 100, 800, 500);
		frmShop.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmShop.getContentPane().setLayout(null);
		
	
		
		time = new JTextField();
		time.setBounds(174, 16, 116, 22);
		frmShop.getContentPane().add(time);
		time.setColumns(10);

		JLabel lblNewLabel = new JLabel("Durata Simularii");
		lblNewLabel.setBounds(12, 16, 107, 22);
		frmShop.getContentPane().add(lblNewLabel);

		JLabel lblProcessingTimemax = new JLabel("Timp Procesare (max)");
		lblProcessingTimemax.setBounds(12, 51, 132, 22);
		frmShop.getContentPane().add(lblProcessingTimemax);

		JLabel lblProcessingTimemin = new JLabel("Timp Procesare(min)");
		lblProcessingTimemin.setBounds(12, 86, 132, 22);
		frmShop.getContentPane().add(lblProcessingTimemin);

		JLabel lblNumarCase = new JLabel("Numar Case");
		lblNumarCase.setBounds(12, 121, 132, 22);
		frmShop.getContentPane().add(lblNumarCase);

		JLabel lblNumarClienti = new JLabel("Numar Clienti");
		lblNumarClienti.setBounds(12, 156, 132, 22);
		frmShop.getContentPane().add(lblNumarClienti);

		ptMax = new JTextField();
		ptMax.setColumns(10);
		ptMax.setBounds(174, 51, 116, 22);
		frmShop.getContentPane().add(ptMax);

		ptMin = new JTextField();
		ptMin.setColumns(10);
		ptMin.setBounds(174, 86, 116, 22);
		frmShop.getContentPane().add(ptMin);

		serverNo = new JTextField();
		serverNo.setColumns(10);
		serverNo.setBounds(174, 121, 116, 22);
		frmShop.getContentPane().add(serverNo);

		clientsNo = new JTextField();
		clientsNo.setColumns(10);
		clientsNo.setBounds(174, 156, 116, 22);
		frmShop.getContentPane().add(clientsNo);

		JLabel lblNewLabel_1 = new JLabel("LOG Evenimente");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel_1.setBounds(387, 11, 143, 31);
		frmShop.getContentPane().add(lblNewLabel_1);

		JButton btnStart = new JButton("START");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int timeLimit = 0;
				int maxProcessingTime = 0;
				int minProcessingTime = 0;
				int numberOfServers = 0;
				int numberOfClients = 0;
				
				
				timeLimit = Integer.parseInt(time.getText());
				maxProcessingTime = Integer.parseInt(ptMax.getText());
				minProcessingTime = Integer.parseInt(ptMin.getText());
				numberOfServers = Integer.parseInt(serverNo.getText());
				numberOfClients = Integer.parseInt(clientsNo.getText());
				
				frmShop.setVisible(true);
			loadSimManager(timeLimit, maxProcessingTime, minProcessingTime, numberOfServers, numberOfClients);
				
			}
		});
		btnStart.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnStart.setBounds(22, 191, 97, 25);
		frmShop.getContentPane().add(btnStart);

		JButton btnStop = new JButton("LOAD");
		btnStop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				int numberOfServers = 0;
				numberOfServers = Integer.parseInt(serverNo.getText());
				drawServers(numberOfServers);
				frmShop.repaint();
				frmShop.revalidate();
			}
		});
		btnStop.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnStop.setBounds(158, 191, 97, 25);
		frmShop.getContentPane().add(btnStop);

		JLabel lblTimer = new JLabel("TIMER");
		lblTimer.setFont(new Font("Arial Black", Font.BOLD, 14));
		lblTimer.setBounds(572, 18, 56, 16);
		frmShop.getContentPane().add(lblTimer);
		
		timer = new JTextField();
		timer.setFont(new Font("Arial Black", Font.PLAIN, 14));
		timer.setBounds(640, 16, 34, 22);
		frmShop.getContentPane().add(timer);
		timer.setColumns(10);
		timer.setText("0");
		timer.setEditable(false);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(346, 51, 378, 165);
		frmShop.getContentPane().add(scrollPane);
		
		frmShop.setResizable(false);
		frmShop.setResizable(false);
	}
}
